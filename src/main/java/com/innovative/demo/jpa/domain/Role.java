package com.innovative.demo.jpa.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.time.ZonedDateTime;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Role extends Base {

    private static final long serialVersionUID = 1L;


    @Column(nullable = false, length = 50, unique = true)
    private String name;

    @Column(nullable = false, length = 50, unique = true)
    private String displayName;

    @Builder
    public Role(Long id, ZonedDateTime createdDate, Long version,
                String name, String displayName) {
        super(id, createdDate, version);
        this.name = name;
        this.displayName = displayName;
    }
}
