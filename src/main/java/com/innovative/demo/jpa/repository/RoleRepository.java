package com.innovative.demo.jpa.repository;

import com.innovative.demo.jpa.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;


public interface RoleRepository extends JpaRepository<Role, Long> {

    @Transactional
    void deleteAllByIdIn(List<Long> idList);

    Optional<Role> findByName(String name);
}
