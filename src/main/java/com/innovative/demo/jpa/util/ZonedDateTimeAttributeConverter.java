package com.innovative.demo.jpa.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Timestamp;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Converter(autoApply = true)
public class ZonedDateTimeAttributeConverter implements AttributeConverter<ZonedDateTime, Timestamp> {

    @Override
    public Timestamp convertToDatabaseColumn(ZonedDateTime value) {
        if (null != value) {
            return Timestamp.from(value.toInstant());
        }
        return null;
    }

    @Override
    public ZonedDateTime convertToEntityAttribute(Timestamp value) {
        if (null != value) {
            return ZonedDateTime.of(value.toLocalDateTime(), ZoneOffset.UTC);
        }
        return null;
    }

}
