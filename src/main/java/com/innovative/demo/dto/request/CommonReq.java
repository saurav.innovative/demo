package com.innovative.demo.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonReq implements Serializable {

    public static final long serialVersionUID = 1L;

    private Long id;
}
