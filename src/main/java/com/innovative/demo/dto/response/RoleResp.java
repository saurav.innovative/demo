package com.innovative.demo.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleResp extends CommonResp {

    public static final long serialVersionUID = 1L;

    private String name;
    private String displayName;

}
