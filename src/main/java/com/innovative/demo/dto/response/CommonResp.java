package com.innovative.demo.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResp implements Serializable {

    public static final long serialVersionUID = 1L;

    private Long id;
    private ZonedDateTime createdDate;
    private Long version;
}
