package com.innovative.demo.service;

import com.innovative.demo.jpa.domain.Role;
import com.innovative.demo.dto.request.RoleReq;
import com.innovative.demo.dto.response.RoleResp;
import com.innovative.demo.jpa.repository.RoleRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Service
public class RoleServiceImpl implements RoleService {

    @NonNull
    private final RoleRepository roleRepository;
    @NonNull
    private final ModelMapper modelMapper;


    @Override
    public Role save(RoleReq roleReq) {
        return roleRepository.save(this.getNewRole(roleReq));
    }

    @Override
    public List<Role> bulkSave(List<RoleReq> roleReqList) {
        return roleRepository.saveAll(roleReqList.stream()
                .map(this::getUpdatedRole).collect(Collectors.toList())
        );
    }

    @Override
    public Role update(RoleReq roleReq) {
        return roleRepository.save(this.getUpdatedRole(roleReq));
    }

    @Override
    public List<Role> bulkUpdate(List<RoleReq> roleReqList) {
        return roleRepository.saveAll(roleReqList.stream()
                        .map(this::getUpdatedRole)
                        .collect(Collectors.toList())
        );
    }

    @Override
    public void delete(Long id) {
        roleRepository.deleteById(id);
    }

    @Override
    public void bulkDelete(List<Long> idList) {
        roleRepository.deleteAllByIdIn(idList);
    }

    @Override
    public RoleResp findById(Long id) {
        return modelMapper.map(roleRepository.findById(id).orElseThrow(NoSuchElementException::new), RoleResp.class);
    }

    @Override
    public RoleResp findByName(String name) {
        return modelMapper.map(roleRepository.findByName(name).orElseThrow(NoSuchElementException::new), RoleResp.class);
    }

    private Role getNewRole(RoleReq roleReq) {
        return Role.builder()
                .createdDate(ZonedDateTime.now())
                .name(roleReq.getName())
                .displayName(roleReq.getDisplayName())
                .build();
    }

    private Role getUpdatedRole(RoleReq roleReq) {
        Role role = roleRepository.findById(roleReq.getId()).orElseThrow(NoSuchElementException::new);
        role.setName(roleReq.getName());
        role.setDisplayName(roleReq.getDisplayName());
        return role;
    }
}
