package com.innovative.demo.service;

import com.innovative.demo.jpa.domain.Role;
import com.innovative.demo.dto.request.RoleReq;
import com.innovative.demo.dto.response.RoleResp;

import java.util.List;

public interface RoleService {

    Role save(RoleReq roleReq) throws Exception;

    List<Role> bulkSave(List<RoleReq> roleReqList) throws Exception;

    Role update(RoleReq roleReq) throws Exception;

    List<Role> bulkUpdate(List<RoleReq> roleReqList) throws Exception;

    void delete(Long id) throws Exception;

    void bulkDelete(List<Long> idList) throws Exception;

    RoleResp findById(Long id) throws Exception;

    RoleResp findByName(String name) throws Exception;
}
