package com.innovative.demo.service;


import com.innovative.demo.jpa.domain.Role;
import com.innovative.demo.dto.request.RoleReq;
import com.innovative.demo.dto.response.RoleResp;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.NoSuchElementException;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SpringBootTest
public class RoleServiceTest {

    private static Long existingIdFor_ADMIN = null;

    @Autowired
    private RoleService roleService;

    @Test
    public void test1FindByName_RoleWithName_ADMIN() {
        try {
            RoleResp resp = roleService.findByName("ADMIN");
            existingIdFor_ADMIN = resp.getId();
            assertNotNull(existingIdFor_ADMIN);
        } catch (Exception ex) {
            ex.printStackTrace();
            assertTrue(ex instanceof NoSuchElementException);
        }
    }

    @Test
    public void test2Delete_ExistingRoleWithName_ADMIN() {
        try {
            roleService.delete(existingIdFor_ADMIN);
        } catch (Exception ex) {
            assertTrue(ex instanceof InvalidDataAccessApiUsageException);
        }
    }

    @Test
    public void test3Save_RoleWithName_ADMIN() {
        RoleReq given = new RoleReq();
        given.setName("ADMIN");
        given.setDisplayName("Administrator");
        try {
            Role role = roleService.save(given);
            existingIdFor_ADMIN = role.getId();
            assertNotNull(existingIdFor_ADMIN);
            assertEquals(role.getName(), given.getName());
        } catch (Exception ex) {
            assertTrue(ex instanceof DataIntegrityViolationException);
        }
    }

    @Test
    public void test4Update_RoleWithName_ADMIN() {
        final String givenName = "ADMIN";
        final String givenDisplayName = "Administrative Role";
        RoleReq given = new RoleReq();
        given.setId(existingIdFor_ADMIN);
        given.setName(givenName);
        given.setDisplayName(givenDisplayName);
        try {
            Role role = roleService.update(given);
            assertEquals(role.getDisplayName(), givenDisplayName);
        } catch (Exception ex) {
            assertTrue(ex instanceof NoSuchElementException || ex instanceof InvalidDataAccessApiUsageException);
        }
    }

    @Test
    public void test5FindById_RoleWithName_ADMIN() {
        try {
            final String givenName = "ADMIN";
            RoleResp roleResp = roleService.findById(existingIdFor_ADMIN);
            assertNotNull(roleResp);
            assertEquals(roleResp.getName(), givenName);
        } catch (Exception ex) {
            assertTrue(ex instanceof NoSuchElementException || ex instanceof InvalidDataAccessApiUsageException);
        }
    }


}
